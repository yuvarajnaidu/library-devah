<?php
require_once('../../php/connection.php');

session_start(); 


if(isset($_POST['functionCall']) && !empty($_POST['functionCall'])) {
    $functionCall = $_POST['functionCall'];
    switch($functionCall) {
        case 'data' : data();break;
        case 'remove' : remove();break;
        case 'userList' : userList();break;
        case 'bookList' : bookList();break;
        case 'removeBook' : removeBook();break;
        case 'userFeedbackList' : userFeedbackList();break;
        case 'systemFeedbackList' : systemFeedbackList();break;
        case 'checkSession' : checkSession();break;
    }
}

function checkSession(){
    if(isset($_SESSION["sessionKey"])){
        if(isset($_SESSION["role"])){
            if($_SESSION["role"]=="role_admin"){
                $response = array( 'status'=> true, 'message' => "Aunthenticated");
                echo json_encode($response);
                exit();
            }else{
                $response = array( 'status'=> false, 'message' => "Not Accessible", 'code' => 401);
                echo json_encode($response);
                exit();
            }
        }else{
            $response = array( 'status'=> false, 'message' => "Not Aunthenticated", 'code' => 401 );
            echo json_encode($response);
            exit();
        }

    }else{
        $response = array( 'status'=> false, 'message' => "Not Aunthenticated", 'code' => 401 );
        echo json_encode($response);
        exit();
    }
}

function data(){
    $conn = connectDB();
    $userCount = $conn->query("SELECT COUNT(*) FROM user where role='role_user' AND active=true");
    $userCount = $userCount->fetch_row();
    $bookCount = $conn->query("SELECT COUNT(*) FROM book where active=true");
    $bookCount = $bookCount->fetch_row();
    $systemFeedback = $conn->query("SELECT COUNT(*) FROM feedback where target_id IS NULL");
    $systemFeedback = $systemFeedback->fetch_row();
    $userFeedback = $conn->query("SELECT COUNT(*) FROM feedback where target_id IS NOT NULL");
    $userFeedback = $userFeedback->fetch_row();

    $bookCountByStatus = mysqli_query($conn,"SELECT count(*) AS total,
    sum(case when book_status = 'Exchange' then 1 else 0 end) AS ExchangeCount,
    sum(case when book_status = 'GiveAway' then 1 else 0 end) AS GiveAwayCount,
    sum(case when book_status = 'Needed' then 1 else 0 end) AS NeededCount
     FROM book");
    $data=mysqli_fetch_assoc($bookCountByStatus);

    $book = array( 'exchangeCount'=> $data['ExchangeCount'], 'giveAwayCount' => $data['GiveAwayCount'], 'neededCount' => $data['NeededCount'] );


    $response = array( 'status'=> true, 'message' => 'success','usersTotal'=> $userCount[0], 'bookTotal'=> $bookCount[0],'systemFeedbackTotal'=> $systemFeedback[0], 'userFeedbackTotal'=> $userFeedback[0], 'pieData'=> json_encode($book) );
    echo json_encode($response);
    $conn->close();
    
}

function userList(){
    $conn = connectDB();
    $length = $_POST['length'];
    $offset = $_POST['offset'];
    $search = $_POST['search'];
    $getAll = "SELECT *, ROW_NUMBER() OVER(ORDER BY user_id) AS Row_Number FROM (SELECT * from user where active=true) xx  WHERE username LIKE '%$search%' OR email_address LIKE '%$search%' LIMIT $length OFFSET $offset ";
    $result = $conn->query($getAll);
    $count = $conn->query("SELECT COUNT(*) FROM (SELECT * from user where active=true) xx WHERE username LIKE '%$search%' OR email_address LIKE '%$search%' LIMIT $length OFFSET $offset ");
    $row = $count->fetch_row();
    $response = array( 'status'=> true, 'message' => 'success', 'data'=>mysqli_fetch_all($result, MYSQLI_ASSOC), 'recordsTotal'=> $row[0] , 'recordsFiltered'=> $row[0]);
    echo json_encode($response);
    $conn->close();
}

function remove(){
    $conn = connectDB();
    $userId =$_POST["userId"];
    $updateQuery = "UPDATE user SET active=false where user_id='$userId'";

    $updateBookQuery = "UPDATE book SET active=false where user_id='$userId'";

    if ($conn->query($updateQuery) === TRUE &&  $conn->query($updateBookQuery) === TRUE) {
        $response = array( 'status'=> true, 'message' => 'Update Successfull');
        echo json_encode($response);
        exit();
    } else {
        $response = array( 'status'=> false, 'message' => $conn->error);
        echo json_encode($response);
        exit();
    }
}

function userFeedbackList(){
    $conn = connectDB();
    $length = $_POST['length'];
    $offset = $_POST['offset'];
    $search = $_POST['search'];
    $getAll = "SELECT *, ROW_NUMBER() OVER(ORDER BY id) AS Row_Number FROM (SELECT feedback.id, user.username,user.user_id as userId, feedback.feedback, TargetUser.username as targetUsername, TargetUser.user_id as targetId   from feedback INNER JOIN user ON user.user_id=feedback.user_id INNER JOIN user AS TargetUser ON TargetUser.user_id=feedback.target_id where target_id IS NOT NULL) xx  WHERE feedback LIKE '%$search%' OR username LIKE '%$search%' OR targetUsername LIKE '%$search%' LIMIT $length OFFSET $offset ";
    $result = $conn->query($getAll);
    $count = $conn->query("SELECT COUNT(*) FROM (SELECT feedback.id, user.username,user.user_id as userId, feedback.feedback, TargetUser.username as targetUsername, TargetUser.user_id as targetId   from feedback INNER JOIN user ON user.user_id=feedback.user_id INNER JOIN user AS TargetUser ON TargetUser.user_id=feedback.target_id where target_id IS NOT NULL) xx  WHERE feedback LIKE '%$search%' OR username LIKE '%$search%' OR targetUsername LIKE '%$search%'");
    $row = $count->fetch_row();
    $response = array( 'status'=> true, 'message' => 'success', 'data'=>mysqli_fetch_all($result, MYSQLI_ASSOC), 'recordsTotal'=> $row[0] , 'recordsFiltered'=> $row[0]);
    echo json_encode($response);
    $conn->close();
}

function systemFeedbackList(){
    $conn = connectDB();
    $length = $_POST['length'];
    $offset = $_POST['offset'];
    $search = $_POST['search'];
    $getAll = "SELECT *, ROW_NUMBER() OVER(ORDER BY id) AS Row_Number FROM (SELECT feedback.id, user.username,user.user_id as userId, feedback.feedback from feedback INNER JOIN user ON user.user_id=feedback.user_id where target_id IS NULL) xx  WHERE feedback LIKE '%$search%' OR username LIKE '%$search%' LIMIT $length OFFSET $offset ";
    $result = $conn->query($getAll);
    $count = $conn->query("SELECT COUNT(*) FROM (SELECT feedback.id, user.username,user.user_id as userId, feedback.feedback from feedback INNER JOIN user ON user.user_id=feedback.user_id where target_id IS NULL) xx  WHERE feedback LIKE '%$search%' OR username LIKE '%$search%' LIMIT $length OFFSET $offset");
    $row = $count->fetch_row();
    $response = array( 'status'=> true, 'message' => 'success', 'data'=>mysqli_fetch_all($result, MYSQLI_ASSOC), 'recordsTotal'=> $row[0] , 'recordsFiltered'=> $row[0]);
    echo json_encode($response);
    $conn->close();
}

function bookList(){
    $conn = connectDB();
    $length = $_POST['length'];
    $offset = $_POST['offset'];
    $search = $_POST['search'];
    $getAll = "SELECT *, ROW_NUMBER() OVER(ORDER BY id) AS Row_Number FROM (SELECT book.id,book.title, user.username, user.user_id as userId , book.author, book.isbn, book.category, book.book_status, book.book_condition, book.book_image, book.record_create_date, book.year FROM book INNER JOIN user ON user.user_id = book.user_id WHERE book.active=true ) xx WHERE title LIKE '%$search%' OR author LIKE '%$search%' OR isbn LIKE '%$search%' OR category LIKE '%$search%' OR  book_status LIKE '%$search%' OR username LIKE '%$search%' LIMIT $length OFFSET $offset ";
    $result = $conn->query($getAll);
    $count = $conn->query("SELECT COUNT(*) FROM (SELECT book.id,book.title, user.username, user.user_id as userId , book.author, book.isbn, book.category, book.book_status FROM book INNER JOIN user ON user.user_id = book.user_id WHERE book.active=true ) xx WHERE title LIKE '%$search%' OR author LIKE '%$search%' OR isbn LIKE '%$search%' OR category LIKE '%$search%' OR  book_status LIKE '%$search%' OR username LIKE '%$search%'");
    $row = $count->fetch_row();
    $response = array( 'status'=> true, 'message' => 'success', 'data'=>mysqli_fetch_all($result, MYSQLI_ASSOC), 'recordsTotal'=> $row[0] , 'recordsFiltered'=> $row[0]);
    echo json_encode($response);
    $conn->close();
}


function removeBook(){
    $conn = connectDB();
    $bookId =$_POST["id"];
    $updateQuery = "UPDATE book SET active=false where id='$bookId'";
    if ($conn->query($updateQuery) === TRUE) {
        $response = array( 'status'=> true, 'message' => 'Update Successfull');
        echo json_encode($response);
        exit();
    } else {
        $response = array( 'status'=> false, 'message' => $conn->error);
        echo json_encode($response);
        exit();
        
    }
}



?>
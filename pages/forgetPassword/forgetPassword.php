<?php
require_once('../../php/connection.php');

session_start(); 


if(isset($_POST['functionCall']) && !empty($_POST['functionCall'])) {
    $functionCall = $_POST['functionCall'];
    switch($functionCall) {
        case 'resetPassword' : resetPassword();break;
        case 'checkSession' : checkSession();break;
    }
}

function checkSession(){
    if(isset($_SESSION["sessionKey"])){

        if(isset($_SESSION["role"])){
            $response = array( 'status'=> true, 'message' => "Success", 'role'=>$_SESSION["role"] );
            echo json_encode($response);
            exit();
        }else{
            $response = array( 'status'=> false, 'message' => "Not Aunthenticated");
            echo json_encode($response);
            exit();
        }
    }else{
        $response = array( 'status'=> false, 'message' => "Not Aunthenticated");
        echo json_encode($response);
        exit();
    }
}
function resetPassword(){

    if( $_POST["oldPassword"] || $_POST['newPassword'] ) {

        $conn = connectDB();
        $oldPassword = $_POST['oldPassword'];
        $newPassword = $_POST['newPassword'];
        $sessionKey =$_SESSION["sessionKey"];
        $getUserIdQuery = "SELECT user_id FROM login_history where session_key='$sessionKey' limit 1";
        $result = mysqli_query( $conn,  $getUserIdQuery);
        $login_history = $result -> fetch_object();

        if($login_history == null){
            $response = array( 'status'=> false, 'message' => "Session Key Not Available",'code' =>401);
            echo json_encode($response);
            exit();
        }

        $getUserDetails = "SELECT password FROM user where user_id='$login_history->user_id' limit 1";
        $userResult = mysqli_query( $conn,  $getUserDetails);
        $user = $userResult -> fetch_object();


        if($user == null){
            $response = array( 'status'=> false, 'message' => "User Id Cant Find",'code' =>401);
            echo json_encode($response);
            exit();
        }

        if($user->password == $oldPassword){

            $updateQuery = "UPDATE user SET password='$newPassword' where user_id='$login_history->user_id'";
            if ($conn->query($updateQuery) === TRUE) {
                $response = array( 'status'=> true, 'message' => 'Update Successfull');
                echo json_encode($response);
                exit();
            } else {
                $response = array( 'status'=> false, 'message' => $conn->error,'code' =>401);
                echo json_encode($response);
                exit();
                
            }
        }else{
            $response = array( 'status'=> false, 'message' => "Old Password Doesn't Match with database",'code' =>401);
            echo json_encode($response);
            exit();
        }



    }
}




?>
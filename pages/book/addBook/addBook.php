<?php
require_once('../../../php/connection.php');

session_start(); 


if(isset($_POST['functionCall']) && !empty($_POST['functionCall'])) {
    $functionCall = $_POST['functionCall'];
    switch($functionCall) {
        case 'addBook' : addBook();break;
        case 'checkSession' : checkSession();break;
    }
}

function checkSession(){
    if(isset($_SESSION["sessionKey"])){
        $response = array( 'status'=> true, 'message' => "Aunthenticated");
        echo json_encode($response);
        exit();
    }else{
        $response = array( 'status'=> false, 'message' => "Not Aunthenticated", );
        echo json_encode($response);
        exit();
    }
}
function addBook(){

    if( $_POST["title"] || $_POST['author'] || $_POST['isbn'] || $_POST['category'] || $_POST['condition'] 
    || $_POST['status'] || $_POST['bookImage'] || $_POST['bookYear']) {
        $title = $_POST['title'];
        $author = $_POST['author'];
        $isbn = $_POST['isbn'];
        $category = $_POST['category'];
        $condition = $_POST['condition'];
        $status = $_POST['status'];
        $bookYear = $_POST['bookYear'];
        $bookImage = $_POST['bookImage'];
        $conn = connectDB();
        $sessionKey =$_SESSION["sessionKey"];
        $getUserIdQuery = "SELECT user_id FROM login_history where session_key='$sessionKey' limit 1";
        $result = mysqli_query( $conn,  $getUserIdQuery);
        $login_history = $result -> fetch_object();

        if($login_history == null){
            $response = array( 'status'=> false, 'message' => "Session Key Not Available",'code' =>401);
            echo json_encode($response);
            exit();
        }
        $insertQuery = "INSERT INTO book (user_id, title, author, isbn, category, book_status, book_condition,year, book_image, session_key )
                VALUES ('$login_history->user_id','$title','$author','$isbn','$category','$status','$condition',$bookYear, '$bookImage', '$sessionKey')";

        if ($conn->query($insertQuery) != TRUE) {
            $response = array( 'status'=> false, 'message' => $conn->error);
            echo json_encode($response);
            exit();
        }

        $response = array( 'status'=> true, 'message' => "Success");
        echo json_encode($response);
        exit();

        $conn->close();
    }
}




?>
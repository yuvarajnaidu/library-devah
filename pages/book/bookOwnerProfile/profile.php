<?php
require_once('../../../php/connection.php');

    

session_start(); 
if(!isset($_SESSION["sessionKey"])){
    $response = array( 'status'=> false, 'message' => "Empty", 'code'=> 401);
    echo json_encode($response);
    exit();
}


if(isset($_POST['functionCall']) && !empty($_POST['functionCall'])) {
    $functionCall = $_POST['functionCall'];
    switch($functionCall) {
        case 'getPopulateData' : getPopulateData();break;
        case 'getNeededBook' : getNeededBook();break;
        case 'getExchangeBook' : getExchangeBook();break;
        case 'getGiveAwayBook' : getGiveAwayBook();break;
        case 'createFeedback' : createFeedback();break;
    }
}
function getPopulateData(){
    $conn = connectDB();
    $userId =$_POST['userId'];

    $sessionKey =$_SESSION["sessionKey"];
    $getUserIdQuery = "SELECT user_id FROM login_history where session_key='$sessionKey' limit 1";
    $result = mysqli_query( $conn,  $getUserIdQuery);
    $login_history = $result -> fetch_object();

    if($login_history == null){
        $response = array( 'status'=> false, 'message' => "Session Key Not Available",'code' =>401);
        echo json_encode($response);
        exit();
    }

    if($login_history->user_id == $userId){
        $response = array( 'status'=> true, 'message' => "Book Owner",'code' =>201);
        echo json_encode($response);
        exit(); 
    }
    $getUserDetails = "SELECT username,email_address,user_image,about,fullName,phoneNumber,role FROM user where user_id='$userId' limit 1";
    $userResult = mysqli_query( $conn,  $getUserDetails);
    $user = $userResult -> fetch_object();
    if($user == null){
        $response = array( 'status'=> false, 'message' => "User Id Cant Find",'code' =>401);
        echo json_encode($response);
        exit();
    }
    $response = array( 'status'=> true, 'message' => $user, 'role' => $_SESSION["role"]);
    echo json_encode($response);
    exit();
}

function getNeededBook(){
    $conn = connectDB();
    $userId =$_POST['userId'];
    $getNeededBook = "SELECT * FROM book where user_id='$userId' AND book_status='Needed'  AND active=true";
    $result = $conn->query($getNeededBook);
    // mysqli_free_result($result);
    if ($result->num_rows > 0) {
        $response = array( 'status'=> true, 'message' => 'success', 'data'=>mysqli_fetch_all($result, MYSQLI_ASSOC));
        echo json_encode($response);
    } else {
        $response = array( 'status'=> false, 'message' => 'No Data');
        echo json_encode($response);
    }
    $conn->close();
    exit();

 
}

function getExchangeBook(){
    $conn = connectDB();
    $userId =$_POST['userId'];
    $getExchangeBook = "SELECT * FROM book where user_id='$userId' AND book_status='Exchange'  AND active=true";
    $result = $conn->query($getExchangeBook);
    // mysqli_free_result($result);
    if ($result->num_rows > 0) {
        $response = array( 'status'=> true, 'message' => 'success', 'data'=>mysqli_fetch_all($result, MYSQLI_ASSOC));
        echo json_encode($response);
    } else {
        $response = array( 'status'=> false, 'message' => 'No Data');
        echo json_encode($response);
    }
    $conn->close();
    exit();

 
}


function getGiveAwayBook(){
    $conn = connectDB();
    $userId =$_POST['userId'];

    $getGiveAwayBook = "SELECT * FROM book where user_id='$userId' AND book_status='GiveAway' AND active=true";
    $result = $conn->query($getGiveAwayBook);
    // mysqli_free_result($result);
    if ($result->num_rows > 0) {
        $response = array( 'status'=> true, 'message' => 'success', 'data'=>mysqli_fetch_all($result, MYSQLI_ASSOC), 'totalLength'=>$result->num_rows);
        echo json_encode($response);
    } else {
        $response = array( 'status'=> false, 'message' => 'No Data');
        echo json_encode($response);
    }
    $conn->close();
    exit();

 
}

function createFeedback(){

    $conn = connectDB();

    $sessionKey =$_SESSION["sessionKey"];
    $getUserIdQuery = "SELECT user_id FROM login_history where session_key='$sessionKey' limit 1";
    $result = mysqli_query( $conn,  $getUserIdQuery);
    $login_history = $result -> fetch_object();

    if($login_history == null){
        $response = array( 'status'=> false, 'message' => "Session Key Not Available",'code' =>401);
        echo json_encode($response);
        exit();
    }
    $targetId =$_POST['targetId'];
    $feedback =$_POST['feedback'];
    $userId =$login_history->user_id;

    $insertQuery = "INSERT INTO feedback (user_id, target_id, feedback, session_key )
                VALUES ($userId,$targetId,'$feedback','$sessionKey')";

        if ($conn->query($insertQuery) != TRUE) {
            $response = array( 'status'=> false, 'message' => $conn->error);
            echo json_encode($response);
            exit();
        }

        $response = array( 'status'=> true, 'message' => "Success");
        echo json_encode($response);
        $conn->close();
        exit();

}

$response = array( 'status'=> true, 'message' => "Execute");
echo json_encode($response);
exit();
?>
<?php
require_once('../../../php/connection.php');

session_start(); 


if(isset($_POST['functionCall']) && !empty($_POST['functionCall'])) {
    $functionCall = $_POST['functionCall'];
    switch($functionCall) {
        case 'viewBook' : viewBook();break;
        case 'checkSession' : checkSession();break;
        case 'getBook' : getBook();break;
    }
}

function checkSession(){
    if(isset($_SESSION["sessionKey"])){
        $response = array( 'status'=> true, 'message' => "Aunthenticated");
        echo json_encode($response);
        exit();
    }else{
        $response = array( 'status'=> false, 'message' => "Not Aunthenticated", );
        echo json_encode($response);
        exit();
    }
}
function viewBook(){
    $conn = connectDB();
    $length = $_POST['length'];
    $offset = $_POST['offset'];
    $search = $_POST['search'];
    $bookId = $_POST['bookId'];
    $authorCheckBool=filter_var($_POST['authorCheckBool'], FILTER_VALIDATE_BOOLEAN);
    $categoryCheckBool=filter_var($_POST['categoryCheckBool'], FILTER_VALIDATE_BOOLEAN);
    $isbnCheckBool=filter_var($_POST['isbnCheckBool'], FILTER_VALIDATE_BOOLEAN);
    $yearCheckBool=filter_var($_POST['yearCheckBool'], FILTER_VALIDATE_BOOLEAN);
    if($bookId == ''){
        $response = array( 'status'=> true, 'message' => 'success', 'data'=>[], 'recordsTotal'=> 0 , 'recordsFiltered'=> 0);
        echo json_encode($response);
        exit();
    }
    $getBookQuery = "SELECT * FROM book where active=true AND id=$bookId limit 1";
    $result = mysqli_query( $conn,  $getBookQuery);
    $book = $result -> fetch_object();

    if($book == null){
        $response = array( 'status'=> false, 'message' => "Book Not Available",'code' =>401);
        echo json_encode($response);
        exit();
    }

    $year = '';
    $author = '';
    $isbn = '';
    $category = '';
    $yearSql = '';
    $authorSql = '';
    $isbnSql = '';
    $categorySql = '';
    if($authorCheckBool){
        $author=$book->author;
        $authorSql = "AND author= '$author' ";
    }
    if($categoryCheckBool){
        $category=$book->category;
        $categorySql = "AND category= '$category' ";
    }
    if($isbnCheckBool){
        $isbn=$book->isbn;   
        $isbnSql = "AND isbn= '$isbn' ";
    }

    if($yearCheckBool){
        $year=$book->year;
        $yearSql = "AND year= '$year' ";
    }

    $str = $authorSql . $categorySql . $isbnSql . $yearSql;
    $arr = explode(' ',trim($str));
    $finalQuery = '';
    if($arr[0] == "AND"){
        $finalQuery =  preg_replace('/AND/', 'WHERE', $str, 1)."\n"; 

    }

    if($finalQuery == ''){
        $response = array( 'status'=> false, 'message' => 'No Data', 'data'=>[], 'recordsTotal'=> 0 , 'recordsFiltered'=> 0);
        echo json_encode($response);
    }else{
        $getAll = "SELECT * FROM (SELECT * from (SELECT * from book where active=true AND id != $bookId) tt $finalQuery) xx WHERE title LIKE '%$search%' OR author LIKE '%$search%' OR isbn LIKE '%$search%' OR category LIKE '%$search%' OR book_status LIKE '%$search%' OR book_condition LIKE '%$search%'  LIMIT $length OFFSET $offset ";
        $result = $conn->query($getAll);
        $count = $conn->query("SELECT COUNT(*) FROM (SELECT * from (SELECT * from book where active=true AND id != $bookId) tt  $finalQuery) xx WHERE title LIKE '%$search%' OR author LIKE '%$search%' OR isbn LIKE '%$search%' OR category LIKE '%$search%' OR book_status LIKE '%$search%' OR book_condition LIKE '%$search%'");
        $row = $count->fetch_row();
        if ($result->num_rows > 0) {
            $response = array( 'status'=> true, 'message' => $authorCheckBool, 'data'=>mysqli_fetch_all($result, MYSQLI_ASSOC), 'recordsTotal'=> $row[0] , 'recordsFiltered'=> $row[0]);
            echo json_encode($response);
        } else {
            $response = array( 'status'=> false, 'message' => 'No Data', 'data'=>mysqli_fetch_all($result, MYSQLI_ASSOC), 'recordsTotal'=> $row[0] , 'recordsFiltered'=> $row[0]);
            echo json_encode($response);
        }
    }

    $conn->close();
    
}

function getBook(){
    $conn = connectDB();

    $sessionKey =$_SESSION["sessionKey"];
    $getUserIdQuery = "SELECT user_id FROM login_history where session_key='$sessionKey' limit 1";
    $result = mysqli_query( $conn,  $getUserIdQuery);
    $login_history = $result -> fetch_object();

    if($login_history == null){
        $response = array( 'status'=> false, 'message' => "Session Key Not Available",'code' =>401);
        echo json_encode($response);
        exit();
    }

    $getBookQuery = "SELECT * FROM book where active=true AND user_id='$login_history->user_id' ";
    $result = $conn->query($getBookQuery);
    if ($result->num_rows > 0) {
        $response = array( 'status'=> true, 'message' => 'success', 'data'=>mysqli_fetch_all($result, MYSQLI_ASSOC));
        echo json_encode($response);
    } else {
        $response = array( 'status'=> false, 'message' => 'No Data');
        echo json_encode($response);
    }
    $conn->close();
    
}




?>
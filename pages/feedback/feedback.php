<?php
require_once('../../php/connection.php');

    

session_start(); 
if(!isset($_SESSION["sessionKey"])){
    $response = array( 'status'=> false, 'message' => "Empty", 'code'=> 401);
    echo json_encode($response);
    exit();
}


if(isset($_POST['functionCall']) && !empty($_POST['functionCall'])) {
    $functionCall = $_POST['functionCall'];
    switch($functionCall) {
        case 'createFeedback' : createFeedback();break;
    }
}
function createFeedback(){

    $conn = connectDB();

    $sessionKey =$_SESSION["sessionKey"];
    $getUserIdQuery = "SELECT user_id FROM login_history where session_key='$sessionKey' limit 1";
    $result = mysqli_query( $conn,  $getUserIdQuery);
    $login_history = $result -> fetch_object();

    if($login_history == null){
        $response = array( 'status'=> false, 'message' => "Session Key Not Available",'code' =>401);
        echo json_encode($response);
        exit();
    }
    $feedback =$_POST['feedback'];
    $userId =$login_history->user_id;

    $insertQuery = "INSERT INTO feedback (user_id,feedback, session_key )
                VALUES ($userId,'$feedback','$sessionKey')";

        if ($conn->query($insertQuery) != TRUE) {
            $response = array( 'status'=> false, 'message' => $conn->error);
            echo json_encode($response);
            exit();
        }

        $response = array( 'status'=> true, 'message' => "Success");
        echo json_encode($response);
        $conn->close();
        exit();

}


$response = array( 'status'=> true, 'message' => "Execute");
echo json_encode($response);
exit();
?>
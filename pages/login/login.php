<?php
require_once('../../php/connection.php');

session_start(); 


if(isset($_POST['functionCall']) && !empty($_POST['functionCall'])) {
    $functionCall = $_POST['functionCall'];
    switch($functionCall) {
        case 'login' : login();break;
        case 'checkSession' : checkSession();break;
    }
}

function checkSession(){
    if(isset($_SESSION["sessionKey"])){

        if(isset($_SESSION["role"])){
            $response = array( 'status'=> true, 'message' => "Success", 'role'=>$_SESSION["role"] );
            echo json_encode($response);
            exit();
        }else{
            $response = array( 'status'=> false, 'message' => "Not Aunthenticated");
            echo json_encode($response);
            exit();
        }
    }else{
        $response = array( 'status'=> false, 'message' => "Not Aunthenticated");
        echo json_encode($response);
        exit();
    }
}
function login(){

    if( $_POST["username"] || $_POST['password'] ) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        date_default_timezone_set("Asia/Singapore");
        $currentDate = date("Y/m/d G:i:s");
        $getQuery = "SELECT * FROM user where username='$username' limit 1";
        $conn = connectDB();
        $result = mysqli_query( $conn,  $getQuery);
        $user = $result -> fetch_object();

        if($user == null){
            $response = array( 'status'=> false, 'message' => "User Not Found");
            echo json_encode($response);
            exit();
        }

        if($password != $user->password){
            $response = array( 'status'=> false, 'message' => 'Incorrect Password');
            echo json_encode($response);
            exit();
        }

        if($user->active == 0){
            $response = array( 'status'=> false, 'message' => 'User has been removed or blocked. Please  Contact Support');
            echo json_encode($response);
            exit();
        }

        $uniqId = uniqid();
        $insertQuery = "INSERT INTO login_history (session_key, user_id)
                VALUES ('$uniqId','$user->user_id')";

        if ($conn->query($insertQuery) != TRUE) {
            $response = array( 'status'=> false, 'message' => $conn->error);
            echo json_encode($response);
            exit();
        }
        date_default_timezone_set("Asia/Singapore");
        $currentDate = date("Y/m/d G:i:s");
        $updateQuery = "UPDATE user SET record_last_login='$currentDate'
                        WHERE user_id= $user->user_id";

        if ($conn->query($updateQuery) != TRUE) {
            $response = array( 'status'=> false, 'message' => $conn->error);
            echo json_encode($response);
            exit();
        }

        $_SESSION["sessionKey"] = $uniqId; 
        $_SESSION["role"] = $user->role; 
        $response = array( 'status'=> true, 'message' => "Success", 'role'=>$user->role);
        echo json_encode($response);
        exit();

        $conn->close();
    }
}




?>
<?php
require_once('../../php/connection.php');

    

session_start(); 



if(isset($_POST['functionCall']) && !empty($_POST['functionCall'])) {
    $functionCall = $_POST['functionCall'];
    switch($functionCall) {
        case 'getPopulateData' : getPopulateData();break;
        case 'updateProfile' : updateProfile();break;
        case 'getNeededBook' : getNeededBook();break;
        case 'getExchangeBook' : getExchangeBook();break;
        case 'getGiveAwayBook' : getGiveAwayBook();break;
        case 'removeBook' : removeBook();break;
        case 'updateBook' : updateBook();break;
        case 'roleCheck' : roleCheck();break;
    }
}

function roleCheck(){
    if(isset($_SESSION["sessionKey"])){

        if(isset($_SESSION["role"])){
            $response = array( 'status'=> true, 'message' => "Success", 'role'=>$_SESSION["role"] );
            echo json_encode($response);
            exit();
        }else{
            $response = array( 'status'=> false, 'message' => "Not Aunthenticated");
            echo json_encode($response);
            exit();
        }
    }else{
        $response = array( 'status'=> false, 'message' => "Not Aunthenticated");
        echo json_encode($response);
        exit();
    }
}
function getPopulateData(){
    $conn = connectDB();
    $sessionKey =$_SESSION["sessionKey"];
    $getUserIdQuery = "SELECT user_id FROM login_history where session_key='$sessionKey' limit 1";
    $result = mysqli_query( $conn,  $getUserIdQuery);
    $login_history = $result -> fetch_object();

    if($login_history == null){
        $response = array( 'status'=> false, 'message' => "Session Key Not Available",'code' =>401);
        echo json_encode($response);
        exit();
    }

    $getUserDetails = "SELECT username,email_address,user_image,about,fullName,phoneNumber,role FROM user where user_id='$login_history->user_id' limit 1";
    $userResult = mysqli_query( $conn,  $getUserDetails);
    $user = $userResult -> fetch_object();

    if($user == null){
        $response = array( 'status'=> false, 'message' => "User Id Cant Find",'code' =>401);
        echo json_encode($response);
        exit();
    }


    $response = array( 'status'=> true, 'message' => $user);
    echo json_encode($response);
    exit();
}

function updateProfile(){
    $conn = connectDB();
    $sessionKey =$_SESSION["sessionKey"];
    $getUserIdQuery = "SELECT user_id FROM login_history where session_key='$sessionKey' limit 1";
    $result = mysqli_query( $conn,  $getUserIdQuery);
    $login_history = $result -> fetch_object();

    if($login_history == null){
        $response = array( 'status'=> false, 'message' => "Session Key Not Available",'code' =>401);
        echo json_encode($response);
        exit();
    }
    $email = $_POST['email'];
    $about = $_POST['about'];
    $userImage = $_POST['userImage'];
    $fullName = $_POST['fullName'];
    $phoneNumber = $_POST['phoneNumber'];
    $updateQuery = "UPDATE user SET email_address='$email',about='$about',user_image='$userImage',fullName='$fullName',phoneNumber='$phoneNumber' where user_id='$login_history->user_id'";
    if ($conn->query($updateQuery) === TRUE) {
        $response = array( 'status'=> true, 'message' => 'Update Successfull');
        echo json_encode($response);
        exit();
    } else {
        $response = array( 'status'=> false, 'message' => $conn->error);
        echo json_encode($response);
        exit();
        
    }
}

function getNeededBook(){
    $conn = connectDB();
    $sessionKey =$_SESSION["sessionKey"];
    $getUserIdQuery = "SELECT user_id FROM login_history where session_key='$sessionKey' limit 1";
    $result = mysqli_query( $conn,  $getUserIdQuery);
    $login_history = $result -> fetch_object();

    if($login_history == null){
        $response = array( 'status'=> false, 'message' => "Session Key Not Available",'code' =>401);
        echo json_encode($response);
        exit();
    }

    $getNeededBook = "SELECT * FROM book where user_id='$login_history->user_id' AND book_status='Needed'  AND active=true";
    $result = $conn->query($getNeededBook);
    // mysqli_free_result($result);
    if ($result->num_rows > 0) {
        $response = array( 'status'=> true, 'message' => 'success', 'data'=>mysqli_fetch_all($result, MYSQLI_ASSOC));
        echo json_encode($response);
    } else {
        $response = array( 'status'=> false, 'message' => 'No Data');
        echo json_encode($response);
    }
    $conn->close();
    exit();

 
}

function getExchangeBook(){
    $conn = connectDB();
    $sessionKey =$_SESSION["sessionKey"];
    $getUserIdQuery = "SELECT user_id FROM login_history where session_key='$sessionKey' limit 1";
    $result = mysqli_query( $conn,  $getUserIdQuery);
    $login_history = $result -> fetch_object();

    if($login_history == null){
        $response = array( 'status'=> false, 'message' => "Session Key Not Available",'code' =>401);
        echo json_encode($response);
        exit();
    }

    $getNeededBook = "SELECT * FROM book where user_id='$login_history->user_id' AND book_status='Exchange'  AND active=true";
    $result = $conn->query($getNeededBook);
    // mysqli_free_result($result);
    if ($result->num_rows > 0) {
        $response = array( 'status'=> true, 'message' => 'success', 'data'=>mysqli_fetch_all($result, MYSQLI_ASSOC));
        echo json_encode($response);
    } else {
        $response = array( 'status'=> false, 'message' => 'No Data');
        echo json_encode($response);
    }
    $conn->close();
    exit();

 
}


function getGiveAwayBook(){
    $conn = connectDB();
    $sessionKey =$_SESSION["sessionKey"];
    $getUserIdQuery = "SELECT user_id FROM login_history where session_key='$sessionKey' limit 1";
    $result = mysqli_query( $conn,  $getUserIdQuery);
    $login_history = $result -> fetch_object();

    if($login_history == null){
        $response = array( 'status'=> false, 'message' => "Session Key Not Available",'code' =>401);
        echo json_encode($response);
        exit();
    }

    $getNeededBook = "SELECT * FROM book where user_id='$login_history->user_id' AND book_status='GiveAway' AND active=true";
    $result = $conn->query($getNeededBook);


    // mysqli_free_result($result);
    if ($result->num_rows > 0) {
        $response = array( 'status'=> true, 'message' => 'success', 'data'=>mysqli_fetch_all($result, MYSQLI_ASSOC), 'totalLength'=>$result->num_rows);
        echo json_encode($response);
    } else {
        $response = array( 'status'=> false, 'message' => 'No Data');
        echo json_encode($response);
    }
    $conn->close();
    exit();

 
}

function removeBook(){
    $conn = connectDB();
    $sessionKey =$_SESSION["sessionKey"];
    $getUserIdQuery = "SELECT user_id FROM login_history where session_key='$sessionKey' limit 1";
    $result = mysqli_query( $conn,  $getUserIdQuery);
    $login_history = $result -> fetch_object();

    if($login_history == null){
        $response = array( 'status'=> false, 'message' => "Session Key Not Available",'code' =>401);
        echo json_encode($response);
        exit();
    }
    $id = $_POST['id'];
    $updateQuery = "UPDATE book SET active=false where id='$id'";
    if ($conn->query($updateQuery) === TRUE) {
        $response = array( 'status'=> true, 'message' => 'Update Successfull');
        echo json_encode($response);
        exit();
    } else {
        $response = array( 'status'=> false, 'message' => $conn->error);
        echo json_encode($response);
        exit();
        
    }
}

function updateBook(){
    $conn = connectDB();
    $sessionKey =$_SESSION["sessionKey"];
    $getUserIdQuery = "SELECT user_id FROM login_history where session_key='$sessionKey' limit 1";
    $result = mysqli_query( $conn,  $getUserIdQuery);
    $login_history = $result -> fetch_object();

    if($login_history == null){
        $response = array( 'status'=> false, 'message' => "Session Key Not Available",'code' =>401);
        echo json_encode($response);
        exit();
    }
    if( $_POST["title"] || $_POST['author'] || $_POST['isbn'] || $_POST['category'] || $_POST['condition'] 
        || $_POST['status'] || $_POST['bookImage'] || $_POST['id'] || $_POST['bookYear']) {
            
            $id = $_POST['id'];
            $title = $_POST['title'];
            $author = $_POST['author'];
            $isbn = $_POST['isbn'];
            $category = $_POST['category'];
            $condition = $_POST['condition'];
            $bookYear = $_POST['bookYear'];
            $status = $_POST['status'];
            $bookImage = $_POST['bookImage'];
            $updateQuery = "UPDATE book SET title='$title',author='$author',isbn='$isbn', category='$category',book_condition='$condition',book_status='$status',book_image='$bookImage',year=$bookYear where id='$id'";
            if ($conn->query($updateQuery) === TRUE) {
                $response = array( 'status'=> true, 'message' => 'Update Successfull');
                echo json_encode($response);
                exit();
            } else {
                $response = array( 'status'=> false, 'message' => $conn->error);
                echo json_encode($response);
                exit();  
            }
        }else{
            $response = array( 'status'=> false, 'message' => 'Data Incomplete');
                echo json_encode($response);
                exit(); 
        }
}


?>